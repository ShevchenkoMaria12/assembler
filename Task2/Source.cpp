#include <stdio.h>
#include <stdlib.h>
#include <iostream>

/*������   ����   ������  ������������  ������  �  ��������  �������������
�����������   ����������   ��  ���������  �������������   ����.   �   �������
������������  �������  ����������  ��������  �������:  ��������  �   ��������
���������� ���������; ������ ���������� �� ������; ��������� ���� �����������
�� ��������, ���������� � ������� (���� ����); ��������, ��������� � �������.
12.  struct Human //�������
	 {
		  char Name[30];
		  int  date;  //����������  ���� �� 1  ������  1900  ����  ��  �������
	 ��������
	 };
������� �� ����� ������ �����, � ������� ��� �������� � ��������� ������.
*/

struct Human {
	char Name[30];
	int  date;
	Human() {}
public:
	char* getName() {
		return Name;
	}
	int getDate() {
		return date;
	}
	void setDate(int date) {
		this->date = date;
	}
};

Human** createArray (int N) {
	__asm {
		mov		ecx, N;
		shl		ecx, 2;
		push	ecx;
		call	malloc;
		add		esp, 4;
		cmp		eax, 0;
		je		FIN;

		mov		ecx, N;
		push	eax;
	CYCLE:
		push	ecx;
		mov		ebx, SIZE Human;
		push	ebx;
		call	malloc;
		add		esp, 4;
		pop		ecx;
		mov		ebx, [esp]; 
		cmp		eax, 0;
		je		CLEAR;

		mov		[ebx][ecx * 4 - 4], eax;
		loop	CYCLE;
		mov		eax, ebx;
		jmp		FIN;
	CLEAR:
		push	ebx;
		call	free;
		add		esp, 4;
	FIN:
		add		esp, 4;
	}
}

void freeArray(Human** arr, int N) {
	__asm {
		mov		eax, arr;
		mov		ecx, N;
	CYCLE:
		push	eax;
		push	ecx;
		mov		edx, [eax][ecx * 4 - 4];
		push	edx;
		call	free;
		add		esp, 4;
		pop		ecx;
		pop		eax;
		loop	CYCLE;

		mov		ebx, arr;
		push	ebx;
		call	free;
		add		esp, 4;
	}
}

void printHuman(Human** arr, int index ) {
	const char* str = "%d - %s	";
	__asm {
		mov		eax, arr;
		mov		edx, index;
		mov		ecx, [eax + edx * 4];
		push	ecx;
		add		ecx, 32;
		mov		ecx, [ecx];
		push	ecx;
		mov		ecx, str;
		push	ecx;
		call	printf;
		add		esp, 12;
	}
};

int birthdayMonth(int N) {
	int months[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int result = 0;
	__asm {
		// ��������
		mov		ecx, 365;
		mov		eax, N;
		mov		edx, 0;
		div		ecx; //� eax ����� �����, � edx �������
		mov		ecx, 4;
		mov		ebx, edx;
		mov		edx, 0;
		div		ecx;
		sub		ebx, eax;
		///// ����� ������ ebx
		lea		eax, months;
		mov		ecx, 12;
		xor		esi, esi; i
	FOR:
		cmp		esi, ecx;
		jge		END_FOR;
		//
		mov		edx, [eax][esi * 4];
		cmp		ebx, edx;
		jbe		BREAK;
		sub		ebx, edx;
		//
		inc		esi;
		jmp		FOR;
	END_FOR:
	BREAK:
		inc		esi;
		mov		result, esi;
	}
	return result;
}

void humanList(Human** arr, int N, int month) {
	__asm {
		mov		ebx, arr;
		mov		ecx, N;
	CYCLE:
		mov		edx, [ebx][ecx * 4 - 4];
		add		edx, 32;
		push	ebx;
		push	ecx;
		dec		ecx;
		push	ecx;
		mov		edx, [edx];
		push	edx;
		call	birthdayMonth;
		add		esp, 4;
		// � eax ����� �������� ��������
		// ecx  ���� �� �����
		mov		ecx, month;
		cmp		eax, ecx;
		jne		FIN;

		mov		ebx, arr;
		push	ebx;
		call	printHuman;
		add		esp, 4;
	FIN: 
		add		esp, 4;
		pop		ecx;
		pop		ebx;
		loop	CYCLE;
	}
}

int main() {
	const int size = 10;
	int N = 0;
	char name[30];
	int month = 0;
	std::cout << "Enter size: " << std::endl;
	std::cin >> N;
	Human** arr = createArray(N);
	for (int i = 0; i < N; i++) {
		std::cout << "Enter Human: " << std::endl;
		std::cin >> arr[i]->date; 
		gets_s(arr[i]->Name);
	}
	std::cout << "Enter month: " << std::endl;
	std::cin >> month;
	std::cout << std::endl;
	std::cout << "List of the people born in " << month << " month:" << std::endl;
	humanList(arr, N, month);
	freeArray(arr, N);
	std::cout << std::endl;
	system("pause");
	return 0;
}
